trigger AObjectTotal on AObject__c (before insert) {
        for(AObject__c ob : Trigger.New){
            if(ob.Total__c == null) {
            	ob.Total__c = 0;
            }
        }
}