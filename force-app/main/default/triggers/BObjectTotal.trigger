trigger BObjectTotal on BObject__c (after insert, after update, after delete, after undelete) {
    
    //List AObjects which must be updated
    List<AObject__c> allAObjects = new List<AObject__c>();
    //Set of ids AObjetc__c's which must be updated because changed Total field  
    Set<Id> ids = new Set<Id>();
    //Map ids associated to AObjects__c's and amout of currency which must be added to Total field
    Map<Id, Decimal> currencies = new Map<Id, Double>();
    
    if(Trigger.isUpdate) {
        ids.clear();
        currencies.clear();
        for(BObject__c ob : Trigger.New) {
            Id newAObject = Trigger.NewMap.get(ob.Id).AObject__c;
            Id oldAObject = Trigger.OldMap.get(ob.Id).AObject__c;
            Double difference = Trigger.NewMap.get(ob.Id).Price__c - Trigger.OldMap.get(ob.Id).Price__c;
            if((difference != 0) && (newAObject == oldAObject)) {
                addToCurrenciesMap(ob.AObject__c, difference);
            } else if((difference != 0) && (newAObject != oldAObject)) {
                addToCurrenciesMap(newAObject, ob.Price__c);
                addToCurrenciesMap(oldAObject, difference - ob.Price__c);
            } else if(newAObject != oldAObject) {
               	addToCurrenciesMap(newAObject, ob.Price__c);
                addToCurrenciesMap(oldAObject, -ob.Price__c);
            }
        }
        updateAObjects();
    } else if(Trigger.isInsert) {
        ids.clear();
        currencies.clear();
        for(BObject__c ob : Trigger.New) {
            addToCurrenciesMap(ob.AObject__c, ob.Price__c);
        }
        updateAObjects();
    } else if(Trigger.isDelete) {
        ids.clear();
        currencies.clear();
        for(BObject__c ob : Trigger.old) {
            addToCurrenciesMap(ob.AObject__c, -ob.Price__c);
        }
        updateAObjects();
    } else {
        ids.clear();
        currencies.clear();
        for(BObject__c ob : Trigger.new) {
            addToCurrenciesMap(ob.AObject__c, ob.Price__c);
        }
        updateAObjects();
    }
    
    private static void addToCurrenciesMap(Id aObjectId, Decimal add) {
        ids.add(aObjectId);
    	if(currencies.containsKey(aObjectId)) {
           	Double price = currencies.get(aObjectId) + add;
            currencies.put(aObjectId, price);           
        } else {
            currencies.put(aObjectId, add);
        }    
    }
    
    private static void updateAObjects() {
        allAObjects = [SELECT Id, Name, Total__c FROM AObject__c WHERE Id in :ids];
        for(AObject__c ob : allAObjects) {
            ob.Total__c = ob.Total__c + currencies.get(ob.Id);
            System.debug(ob.Name);
        }
        System.debug(allAObjects);
        try {
            update allAObjects;
        } catch(Exception ex) {
            ex.getMessage();
        }
    }
}