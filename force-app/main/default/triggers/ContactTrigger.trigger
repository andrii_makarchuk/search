trigger ContactTrigger on Contact (before update) {
    List<Contact> triggerList = new List<Contact>();
    for(Contact cont : Trigger.New) {
        if(Trigger.NewMap.get(cont.Id).SsoId__c != Trigger.OldMap.get(cont.Id).SsoId__c) {
            triggerList.add(cont);
        }
    }
    
    if(!triggerList.isEmpty()) {
        //Load email template
        Custom_Emails_Template__mdt cet = [SELECT Frame_of_Tamplate__c, Reusable_Part__c, Styles__c FROM Custom_Emails_Template__mdt WHERE MasterLabel = : 'News Email' LIMIT 1];
        String html = cet.Frame_of_Tamplate__c.replace('{!Style}', cet.Styles__c);
        //Create set of ECE Numbers for query ECE Objects which will be used in email
        Set<String> eceSet = new Set<String>();
        //Map Contact Id to ECE Numbers
        Map<Id, List<String>> contactIdToECENumbers = new Map<Id, List<String>>();
        for(Contact c : triggerList) {
            List<String> eces = new List<String>();
            for(String str : c.ECE_numbers_and_date__c.split('[\'\\{,\\}\\[\\]]')) {
                if(str != '' && !str.contains('/')) {
                    eceSet.add(str);
                    eces.add(str);
                    System.debug(eces);
                }
            }
            contactIdToECENumbers.put(c.Id, eces);
        }
        System.debug('ECESET ' + eceSet);
        //Extract all ECE Objects for email
        List<ECE__c> ecesList = [SELECT Name, Description__c, Title__c, Image_URL__c FROM ECE__c WHERE Name in: eceSet];
        //Map Contact id to ECE Objects
        System.debug('ECELIST ' + ecesList);
        Map<Id, List<ECE__c>> contactsToECEs = new Map<Id, List<ECE__c>>();
         for(Contact c : triggerList) {
           	 List<ECE__c> eces = new List<ECE__c>();
             for(String s : contactIdToECENumbers.get(c.Id)) {
                 for(ECE__c e : ecesList) {
                     if(e.Name == s) {
                         eces.add(e);
                         break;
                     }
                 }
             }
             contactsToECEs.put(c.Id, eces);
        }
        System.debug('ECEMap ' + contactsToECEs);
        //List email for sending
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for(Contact c : triggerList) {
            if(c.Email != null) {
        		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            	String str = c.Email;
            	List<String> emails = new List<String>();
            	emails.add(str);
            	mail.setToAddresses(emails);
            	mail.setSubject('News'); 
            	mail.setBccSender(false); 
            	mail.setUseSignature(false); 
            	mail.setSenderDisplayName('Salesforce emails'); 
            	mail.setSaveAsActivity(false);
            	String reusablePart = '';
            	for(ECE__c e : contactsToECEs.get(c.Id)) {
                	if(e.Title__c != null && e.Description__c != null && e.Image_URL__c != null) {
						reusablePart = reusablePart + cet.Reusable_Part__c.replace('{!ECE_Number}', e.Name)
                    														.replace('{!Title}', e.Title__c)
                															.replace('{!Description}', e.Description__c)
                    														.replace('{!IMG_src}', e.Image_URL__c);
                	}
            	}
            	String temphtml = html.replace('{!Reusable_Part}', reusablePart);
            	mail.setHtmlBody(temphtml);
            	messages.add(mail);
        	}
        }
        List<Messaging.SendEmailResult> result = Messaging.sendEmail(messages);
        System.debug(result);
        }
    }