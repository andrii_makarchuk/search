
function initMap() {
    var element = document.getElementById('map');
    var options = {
        zoom: 8,
        center: {lat: -34.397, lng: 150.644}
    };
 	var myMap = new google.maps.Map(element, options);
}