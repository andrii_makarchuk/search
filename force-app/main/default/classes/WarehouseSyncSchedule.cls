global with sharing class WarehouseSyncSchedule implements Schedulable {
    //private static final String CRON_exp = '0 0 1 * * ?';
    global void execute(SchedulableContext ctx) {
        WarehouseCalloutService.runWarehouseEquipmentSync();
        //String jobId = System.schedule('WarehouseSyncScheduleTest', CRON_exp, new WarehouseSyncSchedule());
    }
}