public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    @future(callout=true)
    public static void runWarehouseEquipmentSync() {
        
        //ToDo: complete this method to make the callout (using @future) to the
        //      REST endpoint and update equipment on hand.
        Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(WAREHOUSE_URL);
		request.setMethod('GET');
		HttpResponse response = http.send(request);
        System.debug(response.getStatus());
        if(response.getStatusCode() == 200){
        	List<Object> objects = (List<Object>) JSON.deserializeUntyped(response.getBody());
            if(objects.size() > 0)
        		insert makeToProduct(objects);
        }
        //System.debug(response);
    }
    
    private static List<Product2> makeToProduct(List<Object> objects){
        List<Product2> products = new List<Product2>();
        for(Object o : objects){
            Map<String, Object> objectMap = (Map<String, Object>) o;
            Product2 prod = new Product2();
            prod.Cost__c = (Integer) objectMap.get('cost');
            prod.Replacement_Part__c = (Boolean) objectMap.get('replacement');
            Integer ints = (Integer) objectMap.get('quantity');
            prod.QuantityUnitOfMeasure = '' + ints;
            prod.Name = (String) objectMap.get('name');
            prod.Maintenance_Cycle__c = (Decimal) objectMap.get('maintenanceperiod');
            //String ids = (String)objectMap.get('_id');
            //ids = ids.substring(6);
            prod.Lifespan_Months__c = (Decimal) objectMap.get('lifespan');
            prod.StockKeepingUnit = (String) objectMap.get('sku');
            products.add(prod);
        }
        return products;
    }
}