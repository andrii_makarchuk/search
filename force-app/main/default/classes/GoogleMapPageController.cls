public class GoogleMapPageController {
    public String duration {get; private set;}
    public String distance {get; private set;}
    public Direction2Apex result;
    
    
    public String origin;
    public String destination;
    
    public GoogleMapPageController(){
        GoogleMapsCalloutService.doDurationAndDestinationCallout();
        GoogleMapsCalloutService.doDirectionCallout();
        if(GoogleMapsCalloutService.distancematrix != null && GoogleMapsCalloutService.direction != null) {
            JSONParser jp = GoogleMapsCalloutService.distancematrix;
            distance = jp.rows.get(0).elements.get(0).distance.text;
            duration = jp.rows.get(0).elements.get(0).duration.text;
            result = GoogleMapsCalloutService.direction;
            System.debug(GoogleMapsCalloutService.direction);
        }
        System.debug(duration + '   ' + distance); 
        System.debug(result); 
    }
    
    public void doCalculates() {
        GoogleMapsCalloutService.setParams(origin, destination);
        GoogleMapsCalloutService.doDurationAndDestinationCallout();
        GoogleMapsCalloutService.doDirectionCallout();
        if(GoogleMapsCalloutService.distancematrix != null && GoogleMapsCalloutService.direction != null) {
            JSONParser jp = GoogleMapsCalloutService.distancematrix;
            distance = jp.rows.get(0).elements.get(0).distance.text;
            duration = jp.rows.get(0).elements.get(0).duration.text;
            result = GoogleMapsCalloutService.direction;
            System.debug(GoogleMapsCalloutService.direction);
        }
        System.debug(duration + '   ' + distance); 
        System.debug(result); 
    }
    
    public void setOrigin(String s){
        origin = s;
    }
    
    public void setDestination(String s){
        destination = s;
    }
    
    public void setResult(Direction2Apex s){
        result = s;
    }
    
    public String getOrigin(){
        return origin;
    }
    
    public String getDestination(){
        return destination;
    }
    
     public Direction2Apex getResult(){
        return result;
    }
    

}