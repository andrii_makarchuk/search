@isTest
public class BObjectTotal_Test {
    @testSetup
    public static void setup() {
        AObject__c aob = new AObject__c(Name='TestObject');
        insert aob;
        AObject__c aob1 = new AObject__c(Name='TestObject1');
        insert aob1;
        List<BObject__c> s = new List<BObject__c>();
        for(Integer i=0; i<200; i++) {
          	s.add(new BObject__c(Name='Testic'+i, Price__c=100, AObject__c= aob.id));  
        }
        insert s;
    }
    @isTest
    public static void testInsert() {
        System.Test.startTest();
        System.assertEquals(20000, [SELECT id, Total__c FROM AObject__c WHERE Name =:'TestObject' LIMIT 1][0].Total__c);
        System.Test.stopTest();
    }
    @isTest
    public static void testUpdate() {
        List<BObject__c> s = [SELECT id, Price__c FROM BObject__c WHERE Name Like 'Testic%'];
        AObject__c aob = [SELECT id, Total__C FROM AObject__c WHERE Name =:'TestObject1'];
        Decimal sum1 = 0;
        Decimal sum2 = 0;
        for(BObject__c o : s) {
            Decimal price = Math.random()*100;
            if(price < 33){
                o.Price__c = price;
                sum1 = sum1 + o.Price__c;
            } else if(price >= 33 && price <= 66) {
                o.Price__c = price;
                o.AObject__c = aob.id;
                sum2 = sum2 + o.Price__c;
            } else {
                o.AObject__c = aob.id;
                sum2 = sum2 + o.Price__c;
            }
        }
        sum1 = sum1.setScale(10, System.RoundingMode.DOWN);
        sum2 = sum2.setScale(10, System.RoundingMode.DOWN);
        update s;
        System.Test.startTest();
        Decimal res1 = [SELECT id, Total__c FROM AObject__c WHERE Name =:'TestObject' LIMIT 1][0].Total__c.setScale(10, System.RoundingMode.DOWN);
        Decimal res2 = [SELECT id, Total__c FROM AObject__c WHERE Name =:'TestObject1' LIMIT 1][0].Total__c.setScale(10, System.RoundingMode.DOWN);
        System.assertEquals(sum1, res1);
        System.assertEquals(sum2, res2); 
        System.Test.stopTest();
    }
    @isTest
    public static void testDelete() {
        List<BObject__c> s = [SELECT id, Price__c FROM BObject__c WHERE Name Like 'Testic%'];
        delete s;
        System.Test.startTest();
        System.assertEquals(0, [SELECT id, Total__c FROM AObject__c WHERE Name =:'TestObject' LIMIT 1][0].Total__c);
        System.Test.stopTest();
    }
    @isTest
    public static void testUndelete() {
        List<BObject__c> s = [SELECT id, Price__c FROM BObject__c WHERE Name Like 'Testic%'];
        delete s;
        s = [SELECT Id FROM BObject__c WHERE IsDeleted = true ALL ROWS];
        undelete s;
        System.Test.startTest();
        System.assertEquals(20000, [SELECT id, Total__c FROM AObject__c WHERE Name =:'TestObject' LIMIT 1][0].Total__c);
        System.Test.stopTest();
    }
}