global class RSS implements Schedulable{

    public static String CRON_RSS_EXP = '0 0 20 * * ?';
    
    global void execute(SchedulableContext ctx) {
        rssCall();
    }
    
    private static void rssCall() {
        //Extract rss
        Http http = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setEndpoint('https://jyllands-posten.dk/premium/?service=rssfeed&feedtype=rss_2.0&');
        hr.setMethod('GET');
       	HttpResponse hre = http.send(hr);
        System.debug(hre.getStatusCode());
        if(hre.getStatusCode() == 200) {
            String[] items = hre.getBody().split('</item>');
            System.debug(items.size());
            createECEs(items);
        }
    }
    
    private static void createECEs(String[] items) {
        Http http = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setMethod('GET');
       	HttpResponse hre;
        List<ECE__c> eces = new List<ECE__c>();
        //Exctract items from rss and create the ECEs Objects
        for(Integer i = 0; i < items.size() - 2; i++) {
            ECE__c ec = new ECE__c();
            //Replace folder fron f to gallery 
            ec.Image_URL__c = items[i].substringBetween('<media:thumbnail url="', '" ').replace('/f/', '/gallery/');
            if(i < 100) {
                //Сheck is img exist in this folder, if not replace on h-5_3 folder
            	hr.setEndpoint(ec.Image_URL__c);
            	hre = http.send(hr);
            	if(hre.getStatusCode() != 200) {
                	ec.Image_URL__c = ec.Image_URL__c.replace('/gallery/', '/h-5_3/');
            	} 
            }
            ec.Title__c = items[i].substringBetween('<title>', '</title>');
            ec.Description__c = items[i].substringBetween('<description>', '</description>');
            String[] url = items[i].substringBetween('<link>https://jyllands-posten.dk/', '/</link>').split('/');
            ec.Name = url[url.size()-2];
            ec.Date__c = setDate(items[i].substringBetween('<dc:date>', '</dc:date>'));
            eces.add(ec);
        }

        if(!eces.isEmpty()) {
            //Remove ECEs if instance exists 
            eces = dropDuplicates(eces);
            if(!eces.isEmpty()) {
            	insert eces;
            }
        }
        System.debug(eces);
    }
    
    private static DateTime setDate(String dateTag) {
        String[] dateAndTime = dateTag.split('[Z:T-]');
        try {
        	return DateTime.newInstance(Integer.valueof(dateAndTime[0]), Integer.valueof(dateAndTime[1]), Integer.valueof(dateAndTime[2]),
                             Integer.valueof(dateAndTime[3]), Integer.valueof(dateAndTime[4]), Integer.valueof(dateAndTime[5]));
        } catch(Exception ex) {
            System.debug(ex.getStackTraceString());
            return DateTime.now();
        }
    }
    
    private static List<ECE__c> dropDuplicates(List<ECE__c> eces) {
        List<ECE__c> returnList = eces;
        Set<String> numbers = new Set<String>();
        for(ECE__c e : returnList) {
            numbers.add(e.Name);
        }
        Integer disp;
        List<ECE__c> findList = [SELECT Id, Name FROM ECE__c WHERE Name in: numbers];
        if(!findList.isEmpty()) {
            for(ECE__c fes : findList) {
                disp = 0;
                for(Integer i = 0; i < returnList.size(); i++) {
                    if(fes.Name == returnList.get(i - disp).Name) {
                        returnList.remove(i);
                        disp++;
                    }
            	}
            }
        }
        return returnList;
    }
    
}