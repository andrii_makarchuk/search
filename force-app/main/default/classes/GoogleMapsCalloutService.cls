public class GoogleMapsCalloutService {
	
    private static String origins;
    
    private static String destinations;
    
    private static final String key = 'AIzaSyBWqAEUCkGiQWRH9PPyV4bScTPTLGMYhN8';
    
    private static final String myDHttp = 'https://maps.googleapis.com/maps/api/directions/json?';
    
    private static final String myDADHttp = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
    
    private static String urlDAD = 'https://maps.googleapis.com/maps/api/distancematrix/json?' +
        'origins=Seattle&destinations=San+Francisco&key=AIzaSyBWqAEUCkGiQWRH9PPyV4bScTPTLGMYhN8';
    
    private static String urlD = 'https://maps.googleapis.com/maps/api/directions/json?' + 
        'origin=Seattle&destination=San+Francisco&key=AIzaSyBWqAEUCkGiQWRH9PPyV4bScTPTLGMYhN8';
    
    public static JSONParser distancematrix;
    public static Direction2Apex direction;
    
    public static void doDurationAndDestinationCallout() {   
		HttpResponse response = createHttpRequest(urlDAD);
        System.debug(response.getStatus());
        if(response.getStatusCode() == 200){
        	distancematrix = JSONParser.parse(response.getBody());
            if(distancematrix != null)
        		System.debug('obj' + distancematrix);
        }
        //System.debug(response);
        
    }
    
     public static void doDirectionCallout() {   
		HttpResponse response = createHttpRequest(urlD);
        System.debug(response.getStatus());
        if(response.getStatusCode() == 200){
        	direction = Direction2Apex.parse(response.getBody());
            if(direction != null)
        		System.debug('obj' + direction);
        }
        //System.debug(response);
    }
    
    private static HttpResponse createHttpRequest(String url) {
        Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(url);
		request.setMethod('GET');
		return http.send(request);
    }
    
    private static void createURLDAD() {
        urlDAD = myDADHttp + 'origins=' + origins + '&destinations=' + destinations + '&key=' + key;
    }
    
    private static void createURLD() {
        urlD = myDHttp + 'origin=' + origins + '&destination=' + destinations + '&key=' + key;
    }
    
    public static void setParams(String origin, String destination) {
		origins = origin;
		destinations = destination;
		createURLDAD(); 
        createURLD();
    }
}