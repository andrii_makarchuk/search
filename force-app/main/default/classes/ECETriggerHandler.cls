public class ECETriggerHandler implements ITriggerHandler {
	public Boolean IsDisabled()
    {
        return false;
    }
 
    public void BeforeInsert(List<SObject> newItems) {}
 
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {
        Map<Id, ECE__c> eceInAfterInsert = (Map<Id, ECE__c>)newItems;
        Map<Id, String> needInfo = new Map<Id, String>();
    	for(Id eceId : eceInAfterInsert.keySet()) {
            ECE__c ece = eceInAfterInsert.get(eceId);
        	if(ece.Image_URL__c == null || ece.Title__c == null || ece.Description__c == null) {
            	needInfo.put(ece.Id, ece.Name);
        	}
    	}
        
        if(!needinfo.isEmpty()) {
        	makeGetCallout(needInfo);
    	} 
    }
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
 
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    public class PostDetails {
        String title;
        String description;
        String imgUrl;
        PostDetails( String title, String description, String imgUrl) {
            this.title = title;
        	this.description = description;
        	this.imgUrl = imgUrl;
        }
    }
    
    @future(Callout=true)
	private static void makeGetCallout(Map<Id, String> eceNumbers) {
        Map<Id, String> eceNumbersFirst = new Map<Id, String>();
        Map<Id, String> eceNumbersFuture = new Map<Id, String>();
        if(eceNumbers.size() <= 50) {
            eceNumbersFirst = eceNumbers;
        } else {
            Integer i = 0;
            for(Id d : eceNumbers.keySet()) {
                if(i < 50) {
                    eceNumbersFirst.put(d, eceNumbers.get(d));
                } else {
                    eceNumbersFuture.put(d, eceNumbers.get(d));
                }
                i++;
            }
        }
            
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response;
        request.setMethod('GET');
        Map<String, String> statuses = new Map<String, String>();
       	Map<Id, String> eceBodies = new Map<Id, String>();
        for(String d : eceNumbersFirst.keySet()) {
            System.debug('callout:Jyllands_Posten/' + eceNumbersFirst.get(d));
        	request.setEndpoint('callout:Jyllands_Posten/' + eceNumbersFirst.get(d));	
        	response = http.send(request);
        	if (response.getStatusCode() == 301) {
            	request.setEndpoint(response.getHeader('Location'));
                response = http.send(request);
                if (response.getStatusCode() == 200) {
                    System.debug(eceNumbersFirst.get(d) + 'Second Good access');
                    eceBodies.put(d, response.getBody());
                    statuses.put(eceNumbersFirst.get(d) + ' Second Good access', response.getStatus());
                } else {
                    System.debug(eceNumbersFirst.get(d) + 'Second Bad request');
                    statuses.put(eceNumbersFirst.get(d) + '  Second Bad request', response.getStatus());
                }
            } else if(response.getStatusCode() == 200) {
                System.debug(eceNumbersFirst.get(d) + 'First Good request' );
                eceBodies.put(d, response.getBody());
                statuses.put(eceNumbersFirst.get(d) + '  First Good request', response.getStatus());
            } else {
                System.debug(eceNumbersFirst.get(d) + 'First Bad request' );
                statuses.put(eceNumbersFirst.get(d) + '  First Bad request', response.getStatus());
            }
        }
        Map<Id, PostDetails> pd = getDetails(eceBodies);
        List<ECE__c> updateEceList = new List<ECE__c>();
        for(ECE__c e : [SELECT Id, Name, Description__c, Title__c, Image_URL__c FROM ECE__c WHERE Id in: eceBodies.keySet()]) {
            e.Description__c = pd.get(e.Id).description;
            e.Title__c = pd.get(e.Id).title;
            e.Image_URL__c = pd.get(e.Id).imgUrl;
            updateEceList.add(e);
        }
        if(!updateEceList.isEmpty()) {
            update updateEceList;
        }
        
    }
    
    private static Map<Id, PostDetails> getDetails(Map<Id, String> aceBodies){
        System.debug(aceBodies);
        Map<Id, PostDetails> pd = new Map<Id, PostDetails>();
        for(Id d : aceBodies.keySet()) {
            String s = aceBodies.get(d);
            String title = s.substringBetween('<title>', ' - ');
        	String description = s.substringBetween('<meta itemprop="description" content="', '"/>');
        	String imgSrc = s.substringBetween('<meta itemprop="image" content="', '"/>').replace('/b/', '/gallery/');
            PostDetails p = new PostDetails(title, description, imgSrc);
            pd.put(d, p);
        }
        System.debug(pd);
        return pd;
    }
}