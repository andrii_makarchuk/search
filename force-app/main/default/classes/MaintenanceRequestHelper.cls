public with sharing class MaintenanceRequestHelper {
    
    public static void updateWorkOrders() {
        List<Case> caser = [SELECT Id, Type, Status, Vehicle__c, Equipment__c, Equipment__r.Maintenance_Cycle__c, Subject FROM Case WHERE id IN :Trigger.new LIMIT 200];
        List<Case> caseList = new List<Case>();
        for(Case c : caser){
            if(c.Status == 'Closed' && (c.Type == 'Repair' || c.Type == 'Routine Maintenance')){
    			caseList.add(new Case(Vehicle__c=c.Vehicle__c, Equipment__c=c.Equipment__c, Type='Routine Maintenance', Subject=c.Subject,
                                     Date_Reported__c = Date.today(), Date_Due__c=Date.today().addDays(Integer.valueOf(c.Equipment__r.Maintenance_Cycle__c)),
                                     Status='New', Origin='Phone'));            
            }
    	}
		insert caseList;        
    }
}