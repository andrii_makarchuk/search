public class TestClassForLogic {
    public static void ron() {
        for(Integer i = 0; i<20; i++) {
            for(Integer j = 0; j<10; j++) {
                System.debug(i + ' ' + j);
                if(i+j==10) {
                    System.debug('s;dla;sdasd');
                    return;
                }
                    
            }
        }
    }
    
    public static void s() {
        Http http = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setEndpoint('https://my-super-domain-dev-ed.my.salesforce.com/services/oauth2/token?grant_type=authorization_code&code=aPrxGI89zFU.A7D23BrJ.NcZvakDZT9AqMGLwLN3BKM80NfVDV9xYV24qR35ROfSFcKsrEbnCg%3D%3D&client_id=3MVG91BJr_0ZDQ4sLSq3QXEZDY0E3p_L4cJgQzcQ6PWGDq526bQGMMWQQUBvYjs.yLwsKkJMWzCcEnER8weNy&client_secret=E14E675192A3D1AE1DFAD800BEB5F013815F9E70249051E93A6B782FE2188814&redirect_uri=https://openidconnect.herokuapp.com/callback');
        hr.setMethod('POST');
        hr.setHeader('Content-length', '307');
        hr.setHeader('Content-type', 'application/x-www-form-urlencoded');
        	HttpResponse hre = http.send(hr);
    }
    
    public static void rss() {
        Http http = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setEndpoint('https://jyllands-posten.dk/premium/indblik/Sport/ECE11319348/vil-i-have-kaffe-det-drikker-man-jo-meget-af-som-pensionist/');
        hr.setMethod('GET');
       	HttpResponse hre = http.send(hr);
        System.debug(hre.getStatus());
        String s = hre.getBody();
        System.debug(s.length());
        System.debug(hre.getBody());
        System.debug(hre.getHeader('Host'));
        hre.getHeader('Location');
        String title = s.substringBetween('<title>', ' - ');
        System.debug(title);
        String description = s.substringBetween('<meta name="description" content="', '"/>');
        System.debug(description);
        String imgSrc = s.substringBetween('<link rel="image_src" type="image/jpeg" href="', '"/>');
        System.debug(imgSrc);
    }
    
    public static void rssCall() {
        Http http = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setEndpoint('https://jyllands-posten.dk/premium/?service=rssfeed&feedtype=rss_2.0&');
        hr.setMethod('GET');
       	HttpResponse hre = http.send(hr);
        System.debug(hre.getStatusCode());
        if(hre.getStatusCode() == 200) {
            String body = hre.getBody();
            String[] items = body.split('</item>');
            System.debug(items.size());
            createECE(items);
        }
    }
    
    public static void createECE(String[] items) {
        Http http = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setMethod('GET');
       	HttpResponse hre;
        List<ECE__c> eces = new List<ECE__c>();
        for(Integer i = 0; i < items.size() - 2; i++) {
            ECE__c ec = new ECE__c();
            ec.Image_URL__c = items[i].substringBetween('<media:thumbnail url="', '" ').replace('/f/', '/gallery/');
            if(i < 100) {
            	hr.setEndpoint(ec.Image_URL__c);
            	hre = http.send(hr);
            	if(hre.getStatusCode() != 200) {
                	ec.Image_URL__c = ec.Image_URL__c.replace('/gallery/', '/h-5_3/');
            	} 
            }
            ec.Title__c = items[i].substringBetween('<title>', '</title>');
            ec.Description__c = items[i].substringBetween('<description>', '</description>');
            String[] url = items[i].substringBetween('<link>https://jyllands-posten.dk/', '/</link>').split('/');
            ec.Name = url[url.size()-2];
            ec.Date__c = setDate(items[i].substringBetween('<dc:date>', '</dc:date>'));
            eces.add(ec);
        } 
        if(!eces.isEmpty()) {
            insert eces;
        }
        System.debug(eces);
    }
    
    private static DateTime setDate(String dateTag) {
        String[] dateAndTime = dateTag.split('[Z:T-]');
        return DateTime.newInstance(Integer.valueof(dateAndTime[0]), Integer.valueof(dateAndTime[1]), Integer.valueof(dateAndTime[2]),
                             Integer.valueof(dateAndTime[3]), Integer.valueof(dateAndTime[4]), Integer.valueof(dateAndTime[5]));
    }
    
}