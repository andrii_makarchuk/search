public class SearchController {
	public static Map<String, String> labelToName;
    public static List<String> labels {get; set;}
    public static List<Sobject> objects;
    private static String url = 'https://my-super-domain-dev-ed.lightning.force.com/';
    
    private class CustomObject {
        public String name;
        public String href;
        
        public CustomObject(String name, String href) {
            this.name = name;
            this.href = href;
        }
    }
    static {
        labelToName = new Map<String, String>();
        labels = new List<String>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Set<String> keys = schemaMap.keySet();
        for(String str : keys) {
            Schema.DescribeSObjectResult objResult = schemaMap.get(str).getDescribe();
            String name = objResult.getName();
            String label = objResult.getLabel();
            Boolean isCustom = objResult.isCustom();
            if(name.substring(name.length() - 3).equals('__c') && 
               !name.substring(0, name.length() - 3).contains('__')) {
                labelToName.put(label, name);
                labels.add(label);
            }
        }
        for(String ss: labelToName.keySet()) {
            System.debug(ss + '   ' + labelToName.get(ss));
        } 
    }
    
    public SearchController() {    
    }
    
    @AuraEnabled(cacheable=false)
    public static List<String> sobjetcs(String name, String objLabel) {
        System.debug(name +'  '+ objLabel);
        String obj = labelToName.get(objLabel);
        List<Sobject> objList;
            String o = 'SELECT Id, Name FROM ' + obj + ' WHERE Name like \'%' + name + '%\' LIMIT 15';
        	objList = Database.query(o);

        List<String> objs = new List<String>();
        for(Sobject sObj : objList) {
            String s = '{ "name":"'+sObj.get('Name').toString()+'","href":"'+url + sObj.Id+'"}';
            objs.add(s);
        }
        System.debug(objs);
        return objs;
    }
    
    public static Map<String, String> describe() {
        Map<String, Schema.SObjectType> schemaMap2 = Schema.getGlobalDescribe();
        return new Map<String, String>();
    }
    @AuraEnabled
    public static List<String> labels() {
        return labels;
    }
}