public class DropBoxController {
    private final String domainUrl = 'https://my-super-domain-dev-ed--c.visualforce.com/';
    private final String returnUri = 'apex/DropBoxPage';
	private final String appKey = 'qxdtwcej1e72pdh';
    private final String appSecret = 'ugnm6lrlov7wgr5';
    private final String dropBoxUrl = 'https://www.dropbox.com/';
    private final String dropBoxApi = 'https://api.dropboxapi.com/';
    private String domainUrl1 = ApexPages.currentPage().getParameters().toString(); 
    private String accessToken;
    public String response{get; set;}
    public DropBoxController() {
        Map<String, String> pageParams = ApexPages.currentPage().getParameters();
        if(pageParams.size() == 2 && pageParams.get('code') != null) {
            getToken(pageParams.get('code'));
        }
    }
    
    public PageReference registration() {
        PageReference page = new PageReference(String.format('{0}{1}?response_type=code&client_id={2}&redirect_uri={3}&state=RestTest', 
                                                            new String[]{dropBoxUrl, 'oauth2/authorize', appKey, domainUrl + returnUri})); 
        return page;
    }
    
    private void getToken(String code) {
        System.debug(code);
        String getAccessTokenUrl = String.format('{0}{1}?code={2}&grant_type=authorization_code&client_id={3}&client_secret={4}&redirect_uri={5}', 
                                                 new String[]{dropBoxApi, 'oauth2/token', code, appKey, appSecret, domainUrl + returnUri});
        System.debug(domainUrl1);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(getAccessTokenUrl);
        HttpResponse response = http.send(request);
        System.debug(response.getStatus() + '   ' + response.getStatusCode());
        if(response.getStatus() == 'OK') {
       		System.debug(response.getBody());
            accessToken = TokenResponse2Apex.parse(response.getBody()).access_token;
        }
    }
    
    public void createFolder() {
        if(accessToken != null){
 			Http http = new Http();
        	HttpRequest request = new HttpRequest();
        	request.setMethod('POST');
        	request.setEndpoint(dropBoxApi + '2/files/create_folder_v2');
        	request.setHeader('Authorization', 'Bearer ' + accessToken);
            request.setHeader('Content-Type', 'application/json');
            request.setBody('{\"path\": \"/Testometr\",\"autorename\": false}');
            HttpResponse response = http.send(request);
        	System.debug(response.getStatus() + '   ' + response.getStatusCode());
        	if(response.getStatus() == 'OK') {
       			System.debug(response.getBody());
            	//accessToken = TokenResponse2Apex.parse(response.getBody()).access_token;
        	}
        }
    }
}
//https://qxdtwcej1e72pdh:ugnm6lrlov7wgr5@api.dropbox.com/1/metadata/link
//https://www.dropbox.com/oauth2/authorize?client_id=qxdtwcej1e72pdh&response_type=token&redirect_uri=%27%20+%20redirectUri;