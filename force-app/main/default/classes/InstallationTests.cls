// This class can be safely deleted from your org. 
// It was only used to ensure 100% test coverage 
// for the managed package installation 

@IsTest
public class InstallationTests {
    private static final String CRON_exp = '0 0 1 * * ?';
    private static List<Case> createMex(){
        Vehicle__c v = new 	Vehicle__c(Name='Test V');
        insert v;
        Product2 pr = new Product2(Name='Test Eq', Maintenance_Cycle__c=10);
        insert pr;
        List<Case> cases = new List<Case>();
        for(Integer i = 0; i<300; i++)
            cases.add(new Case(Vehicle__c=v.Id, Equipment__c=pr.Id, Type='Repair', Subject='c.Subject'+i,
                                     Date_Reported__c=Date.today(), Status='New', Origin='Phone'));
        
        insert cases;
        List<Case> casesToUpdate = [SELECT Id FROM Case 
                        WHERE Type =:'Repair' AND Status=:'New' AND  Subject like :'c.Subject%' LIMIT 300];
      
        for(Case s : casesToUpdate){
            s.Status = 'Closed';
        }
       	return casesToUpdate;
    }
    
    //@IsTest
    public static void testMaintenanceRequest(){
        List<Case> cases = createMex();
		Test.startTest();
		Database.update(cases);
        Test.stopTest();
		
		List<Case> c = [SELECT Id, Type, Status, Vehicle__c, Equipment__c, Equipment__r.Maintenance_Cycle__c, Subject FROM Case 
                        WHERE Type =:'Routine Maintenance' AND Status=:'New' AND Subject like :'c.Subject%' ];        
        //MaintenanceRequestHelper.updateWorkOrders();
        System.assertEquals(300, c.size());
    }
    
    
    @IsTest
    public static void testWarehouseSync(){
        String tid;
        Product2 pr = new Product2();
        //WarehouseCalloutService.runWarehouseEquipmentSync();
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());
        Test.startTest();
        tid = System.schedule('WarehouseSyncScheduleTestTest', CRON_exp, new WarehouseSyncSchedule());
        CronTrigger ct = [SELECT Id FROM CronTrigger WHERE id = :tid];
        
        System.assertEquals(ct != null, true);

        Test.stopTest();
            }
}