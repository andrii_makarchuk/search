({
    defineWriteCode : function(keyCode) {
        if(keyCode >= 47 && keyCode <= 57 || keyCode == 32 || keyCode == 8) {
            return true;
        } else if(keyCode >= 65 && keyCode <= 90) {
            return true;
        } else if(keyCode >= 96 && keyCode <= 111) {
            return true;
        } else if(keyCode >= 186 && keyCode <= 192) {
            return true;
        } else if(keyCode >= 219 && keyCode <= 222) {
            return true;
        } else {
            return false;
        }
    },
    
    keyboardManipulation : function(keyCode, cmp) {   
        const list = document.getElementById("myDropdown");
        let href = cmp.get('v.currentURL');
        const childs = document.getElementsByClassName("items123");
        let next, prev, current;
        for(let i = 0; i < childs.length; i++) {
            if(childs[i].getAttribute('href') == href) {
                current = childs[i];
                if(i+1 == childs.length){
                	next = childs[0];
                } else {
                    next = childs[i+1]; 
                }
                if(i-1 == -1){
                    prev = childs[childs.length - 1];
                } else { 
                    prev = childs[i-1]; 
                }
            } 
        }
        if(href == null) {
            next = childs[0];
            prev = childs[childs.length - 1];
        }
        if(keyCode == 38) {
            cmp.set('v.currentURL', prev.getAttribute('href'));
            href = prev.getAttribute('href');
            prev.classList.add("buttonHover");
            if(current != null) {
                current.classList.remove("buttonHover");
            }  
        } else if(keyCode == 40) {
            cmp.set('v.currentURL', next.getAttribute('href'));
            href = next.getAttribute('href');
            next.classList.add("buttonHover");
            if(current != null) {
                current.classList.remove("buttonHover");
            }
        } else if(keyCode == 13) {
            if(href != null) {
                window.location.href = href;
            }
        } 
    },
    
	addSobjectLabel : function(customObjects) {
		let options = [];
        for(let i = 0; i < customObjects.length; i++) {
            const node = document.createTextNode(customObjects[i]);
            const option = document.createElement("option");
            option.appendChild(node);
            options[i] = option;
        }
        const list = document.getElementById("mySelect");
        for(let i = 0; i < options.length; i++) {
            list.appendChild(options[i]);
        }
	},
    
    addRecords : function(customObjects, cmp, current) {
        const write = cmp.get("v.write");
        if(write == current) {
			let options = [];
        	for(let i = 0; i < customObjects.length; i++) {
            	const customObject = JSON.parse(customObjects[i]);
            	const node = document.createTextNode(customObject.name);
            	const href = customObject.href;
            	const a = document.createElement("a");
            	a.href = href;
            	a.className = "items123";
            	a.appendChild(node);
            	options[i] = a;
        	}
        	const list = document.getElementById("myDropdown");
       		const loader = document.getElementById("loader");
        	if(loader != null) {
        		list.removeChild(loader);
        	}
        	for(let i = 0; i < options.length; i++) {
            	list.appendChild(options[i]);
        	}
            cmp.set("v.write", 0);
        }
        
	},
    
    removeRecords : function() {      
        const parent = document.getElementById("myDropdown"); 
        const childs = document.getElementsByClassName("items123");
        for(let i = 0; i < childs.length; i++) {
            parent.removeChild(childs[i]);
        }
        let loader = document.getElementById("loader");
        if(loader == null) {
        	let newLoader = document.createElement("div");
        	newLoader.className = "loaders";
        	newLoader.id = "loader";
        	let dropdown = document.getElementById("myDropdown");
        	dropdown.appendChild(newLoader);
        }
    },
             
    salesforceCallD : function(cmp, helper, current) {
        const select = document.getElementById("mySelect");
        const filterS = select.value;
  		const input = document.getElementById("myInput");
  		let filterI = input.value;
        if(filterI == null) {
            filterI = '';
        }
        
        const action = cmp.get("c.sobjetcs");
        action.setParams({name: filterI, objLabel: filterS});
        action.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                helper.addRecords(response.getReturnValue(), cmp, current);
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
                const errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },         
    
    debounce : function(func, wait, cmp) {
    	const vs = function() {    
            let context = this;
            let args = arguments;
            const later = function() {
                cmp.set('v.timeout', null);
                func.apply(context, args);
            };
            
            clearTimeout(cmp.get('v.timeout'));
            let vtimeout = setTimeout(later, wait);
            cmp.set('v.timeout', vtimeout); 
         }
         return vs();
	},
})