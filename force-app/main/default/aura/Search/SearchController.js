({  
	doInit : function(cmp, event, helper) {
      	const action = cmp.get("c.labels");
        action.setCallback(this, function(response) {
            console.log(response.getState());
            const state = response.getState();
            if (state === "SUCCESS") {
                helper.addSobjectLabel(response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
                const errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
		$A.enqueueAction(action);
  	},
     
    filterFunction : function(cmp, event, helper) {
        if(helper.defineWriteCode(event.keyCode) || event.type == "change") {
            cmp.set("v.currentURL", null);
            let write = cmp.get("v.write");
            cmp.set("v.write", ++write);

            if(event.type != "change"){
            	document.getElementById("clearItem").classList.add("show");
            }
           	const input = document.getElementById("myInput").value;
        	if(input == '' || input == null) {
            	document.getElementById("clearItem").classList.remove("show");
        	}  

        	helper.removeRecords();
            const call = function () { 
                setTimeout($A.getCallback(helper.salesforceCallD.bind(this, cmp, helper, write)), 0);
            }; 
        	helper.debounce(call, 750, cmp);
        }
	}, 
    
    add : function() {
  		document.getElementById("myDropdown").classList.add("show");
	},
    
    remove : function() {
        let isSelected = false;
        const aElems = document.getElementsByClassName("items123");
        for(let i = 0; i < aElems.length; i++) {
            aElems[i].addEventListener("click", function() {
                isSelected = true;
            	document.getElementById("myDropdown").classList.remove("show");
            });
        }
        
        window.setTimeout(function() {
  			if(!isSelected){
  				document.getElementById("myDropdown").classList.remove("show");
        	}
        }, 100);
	},
    
    keyboardMoves : function(cmp, event, helper) {
  		helper.keyboardManipulation(event.keyCode, cmp);
	},
    
    clear : function(cmp, event, helper) {
        document.getElementById("clearItem").classList.remove("show");
        
        cmp.set("v.currentURL", null);
        let write = cmp.get("v.write");
        cmp.set("v.write", ++write);

        let clear = document.getElementById("myInput");
        clear.value = '';
        
        helper.removeRecords();
        const call = function () { 
        	setTimeout($A.getCallback(helper.salesforceCallD.bind(this, cmp, helper, write)), 0);
        }; 
        helper.debounce(call, 750, cmp);
    },
})